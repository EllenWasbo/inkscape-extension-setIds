# Inkscape extensions get and setGlyphIDs

These two extensions streamline the process of creating fonts using the Custom Stroke Font extension  https://github.com/Shriinivas/inkscapestrokefont. With a font with many glyphs it might be quite cumbersome to set the ids of each glyph manually using the XML editor. Use the getGlyphIDs to fetch the IDs before editing a font - the path IDs are easily changed during editing. Use the setGlyphIDs to set the path IDs effectively based on a string input having all the glyphs ordered from left to right.

Read more about this extension on my blog [Cutlings](http://cutlings.wasbo.net/inkscape-extension-automate-glyph-ids/).

The extension will appear in Extensions -> Cutlings

Tested with Inkscape v1.1 on Windows 10


